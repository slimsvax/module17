// Module17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cmath>


class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << x << " " << y << " " << z << '\n';
    }
    void Length()
    {
        std::cout << "Vector length = " << sqrt(x*x + y*y + z*z) << '\n';
    }
private:
    double x;
    double y;
    double z;

};










int main()
{
    Vector v(5, 5, 5);
    v.Show();
    v.Length();
}

